var app = require('./app/app.js');
// set the preferred port for app to listen to
var port = 1234;

// run the server, listening on preferred port
app.listen(port);
console.log(`the server is running on port: ${port}  ...`);
