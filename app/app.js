// import express
var express = require('express');
// allow cross origin data requests to serve all kinds of clients: web, mobile platforms, etc..
const cors = require('cors');
// initialize express app
app = express();

var mongoose = require('mongoose');
var config = require('../config/config.js');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var passport = require('passport');


// enable cors  (cross origin requests)
app.use(cors({
	'origin': '*',
	'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
	'preflightContinue': false
}));
app.options('*', cors());

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});


// using body-parser lib to parse  POST params
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// initializing the logging  with morgan lib
app.use(morgan('dev'));

// initialize passport.js
app.use(passport.initialize());

// connect to Mongo  database
mongoose.connect(config.database);

// add routes from USER controller for auth, signedUp
app.use('/api', require('./controllers/userController.js'));

// add routes from todo_ controller for CRUD
app.use('/api', require('./controllers/todoController.js'));

// home    test route
app.get('/', function (req, res) {
	res.send(' home page ');
});

// app.listen(port);
// console.log(`the server is running on port: ${port}  ...`);

module.exports = app;