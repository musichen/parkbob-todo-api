var mongoose = require('mongoose');
Schema = mongoose.Schema;
var Todo = require('./todo.js'); // import Todo_ schema to reference in User schema as one to many relation
var bcrypt = require('bcrypt');

// definition of USER schema

var UserSchema = new mongoose.Schema({
	email: {
		type: String,
		lowercase: true,
		unique: true,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	todos: [{
		type: Schema.Types.ObjectId,
		ref: 'Todo'
	}]
});

// avoid stroing users pass, by storing "salted" encrypted passwords using bcrypt lib
UserSchema.pre('save', function (next) {
	var user = this;
	if (this.isModified('password') || this.isNew) {
		bcrypt.genSalt(10, function (err, salt) {
			if (err) return next(err);

			bcrypt.hash(user.password, salt, function (err, hash) {
				if (err) return next(err);

				user.password = hash;
				next();
			});
		});
	} else {
		return next();
	}
});

// Create method to perform password comparison

UserSchema.methods.comparePassword = function (password, cb) {
	bcrypt.compare(password, this.password, function (err, isMatch) {
		if (err) return cb(err);

		cb(null, isMatch);

	});
}

module.exports = mongoose.model('User', UserSchema);
