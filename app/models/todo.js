var mongoose = require('mongoose');
var User = require('./todo.js'); // import User schema to reference in Todo_ schema as many to one relation
// definition of _TODO schema

var TodoSchema = new mongoose.Schema({
	task: {
		type: String,
		required: true
	},
	done: {
		type: Boolean,
		default: false
	},
	updated_at: {
		type: Date,
		default: Date.now
	},
	owner: {type: Schema.Types.ObjectId, ref: 'User'}
});


module.exports = mongoose.model('Todo', TodoSchema);
