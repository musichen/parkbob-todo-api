var config = require('../../config/config.js');
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
// import Todo_ Model
var Todo = require('../models/todo.js');
// import User Model
var User = require('../models/user.js');
// apply the passport strategy
var passport = require('passport');
require('../../config/passport.js')(passport);
ObjectId = require('mongodb').ObjectID;
var _ = require('lodash');

/* TODO_ CRUD ACTIONS */

/*
 var aaron = new Person({ _id: 0, name: 'Aaron', age: 100 });
 aaron.save(function (err) {
 if (err) return handleError(err);

 var story1 = new Story({
 title: "Once upon a timex.",
 _creator: aaron._id    // assign the _id from the person
 });

 story1.save(function (err) {
 if (err) return handleError(err);
 // thats it!
 });

 //then add story to person
 aaron.stories.push(story1);
 aaron.save(callback);
 });

*/


/* CREATE new TODO_ */
router.post('/todo/add', passport.authenticate('jwt', { session: false}), (req, res) => {

	User.findOne({
		email: req.user.email
	}, function(err, user) {
		if (err) throw err;
		if (!user) {
			console.log("LOG: User does not exist.", err);
		} else {
			console.log("LOG: User found ", user);

			// create a new TODO_ and assign owner to user, that initiated the creation

			var newTodo = new Todo({
				task: req.body.task,
				done: req.body.done,
				owner: req.user.id
			});
			newTodo.save((err, newTodo) => {
				if (err) { console.log(err); }
				else {
					console.log("LOG: Todo created successfuly.");

					// push the todo_ into todo_ field of user object
					user.todos.push(newTodo._id);
					user.save((err, updatedUser) => {
						if (err) {
							console.log(err);
						} else {
							console.log("LOG: updated user with new todo. ", updatedUser);
							res.status(201).json({success: true, message: "todo created successfully", todo: newTodo});
						}
					});
				}
			});


		}
	});

});

/* LIST ALL TODOs that belong authenticated agent that makes request */
router.get('/todos', passport.authenticate('jwt', { session: false}), (req, res) => {
	Todo.find({owner: req.user.id}, (err, foundTodos) => {
		if (err) {
			console.log(err);
			res.status(204).json({success: false, message: "no todos found for this user"});
		} else {
			res.status(200).json({success: true, message: "todos have been found. ", todos: foundTodos});
		}
	});
});

/* READ (GET) TODO_ by :id */
router.get('/todo/:id', passport.authenticate('jwt', { session: false}), (req, res) => {
	Todo.findOne({owner: req.user.id , _id: ObjectId(req.params.id) }, (err, foundTodo) => {
		if (err) { console.log(err); res.json({success: false, message: "there was en error", err: err}); }
		if (!foundTodo) {
			res.status(204).json({success: false, message: "todo not found. "});
		} else {
			res.status(200).json({success: true, message: "todo found. ", todo: foundTodo});
		}
	});
});

/* UPDATE TODO_ by :id */
router.put('/todo/edit/:id', passport.authenticate('jwt', { session: false}), (req, res) => {
	var task = req.body.task;
	var done = req.body.done;
	Todo.findOne({owner: req.user.id , _id: ObjectId(req.params.id) }, (err, foundTodo) => {
		if (err) { console.log(err); res.json({success: false, message: "there was en error", err: err}); }
		if (!foundTodo) {
			res.status(204).json({success: false, message: "todo not found. "});
		} else {
			if (task) foundTodo.task = task;
			if (typeof(done) !== typeof undefined && (done === 'true' || done === 'false')) foundTodo.done = (done === "true");
			foundTodo.updated_at = Date.now();
			foundTodo.save((err, updatedTodo) => {
				if (err) { console.log(err); }
				else {
					console.log("LOG: updated todo. ", updatedTodo);
					res.status(200).json({success: true, message: "todo updated. ", todo: foundTodo});
				}
			});
		}
	});
});

/* COMPLETE (CHECK) TODO_  */
router.get('/todo/check/:id', passport.authenticate('jwt', { session: false}), (req, res) => {
	Todo.findOne({owner: req.user.id , _id: ObjectId(req.params.id) }, (err, foundTodo) => {
		if (err) { console.log(err); res.json({success: false, message: "there was en error", err: err}); }
		if (!foundTodo) {
			res.status(204).json({success: false, message: "todo not found. "});
		} else {
			foundTodo.done = !(foundTodo.done);
			foundTodo.updated_at = Date.now();
			foundTodo.save((err, updatedTodo) => {
				if (err) { console.log(err); }
				else {
					console.log("LOG: todo check toggled! ", updatedTodo);
					res.status(200).json({success: true, message: "todo check toggled. ", todo: foundTodo});
				}
			});
		}
	});
});


/* DELETE TODO_ by :id */
router.delete('/todo/:id', passport.authenticate('jwt', { session: false}), (req, res) => {
	Todo.findOne({owner: req.user.id , _id: ObjectId(req.params.id) }, (err, foundTodo) => {
		if (err) { console.log(err); res.status(500).json({success: false, message: "there was en error", err: err}); }
		if (!foundTodo) {
			res.status(204).json({success: false, message: "todo not found. "});
		} else {
			var todoId = foundTodo._id;
			foundTodo.remove((err, deletedTodo) => {
				if (err) { console.log(err); }
				else {
					// find associated user that created this Todo_
					User.findOne({ email: req.user.email }, function(err, user) {
						if (err) throw err;
						if (!user) {
							console.log("LOG: USER NOT FOUND ==> ", err);
						} else {
							// remove todo_ from associated and user and update it
							_.remove(user.todos, (id) => {
								return id === todoId;
							});
							user.save((err, updatedUser) => {
								if (err) { console.log(err);
								} else {
									console.log("LOG: deleted todo. ", deletedTodo);
									res.status(200).json({success: true, message: "todo deleted. ", todo: deletedTodo});
								}
							});
						}
					});
				}
			});

		}
	});
});

module.exports = router;
