var config = require('../../config/config.js');
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/user.js');
var passport = require('passport');
// apply the passport strategy
require('../../config/passport.js')(passport);
var jwt = require('jsonwebtoken');

// Register new user
router.post('/signup', function(req, res) {
   if (!req.body.email || !req.body.password) {
     res.status(400).json({ success: false, message: 'enter email and password to register'});
   } else {
     var newUser = new User({
       email: req.body.email,
       password: req.body.password
     });

     // try to save the user
     newUser.save(function (err, saveduser) {
       if (err) return res.status(409).json({success: false, message: 'email already exists', err: err});
       res.status(201).json({
         success: true,
         message: `new user  ${saveduser.email}  created successfully`,
         useremail: saveduser.email
       });
     });
   }
});

// authenticate the user and get a JWT instance
router.post('/auth', function(req, res) {
	console.log(req.body.email);
  User.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) throw err;
    if (!user) {
      res.status(401).json({success: false, message: 'authentication failed. user with this email not found.'});
    } else {
      // password match checking
      user.comparePassword(req.body.password, function(err, isMatch) {
        if (isMatch && !err) {
          // create json web token   if no errors and match
          var token = jwt.sign(user, config.secret, {
            expiresIn: 10080 // week in seconds
          });
          res.status(200).json({
            success: true,
            token: `JWT ${token}`,
            message: `successfully authenticated as ${user.email} .`,
            useremail: user.email
          });
        } else {
          res.status(401).json({success: false, message: 'authentication failed. passwords do not match.'})
        }
      });
    }
  });
});

/*
// Protect dashboard route with JWT
router.get('/mytodo', passport.authenticate('jwt', { session: false}), function(req, res) {
  res.json({ success: true, message: `authenticated! User ${req.user.email} with id: ${req.user.id} .`});
});*/

module.exports = router;
