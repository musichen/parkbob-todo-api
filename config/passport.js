var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var User = require('../app/models/user.js');
var config = require('../config/config.js');

module.exports = function(passport) {
  var opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
  opts.secretOrKey = config.secret;

  passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    // console.log("__ _ __  ___ - ___ _ JWT_PAYLOAD .$__._id: ", jwt_payload.$__._id);

    User.findOne({_id: jwt_payload.$__._id}, function (err, user) {
      if (err) return done(err, false);
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    });
  }));
};
