let NODE_ENV = process.env.NODE_ENV;
if (!NODE_ENV) {
	console.log(" The NODE_ENV environment variable  was not specified. Fallback to default NODE_ENV=development");
	NODE_ENV = "development";
	/* throw new Error(
		'The NODE_ENV environment variable is required but was not specified.'
	); */
}

/* configure the backend API HOST AND PORT */

/* development */
const developmentDbUrl = 'mongodb://appdbuser:v6Fu27lvxv3a@ds129733.mlab.com:29733/tododb';
/* local */
const localDbUrl = 'mongodb://appdbuser:v6Fu27lvxv3a@127.0.0.1:27017/tododb';
/* test */
const testDbUrl = 'mongodb://appdbuser:v6Fu27lvxv3a@127.0.0.1:27017/tododb';
/* production */
const productionDbUrl = 'mongodb://appdbuser:v6Fu27lvxv3a@ds129733.mlab.com:29733/tododb';
/* rpi   deployment on raspberry pi*/
const raspberryPi = 'mongodb://appdbuser:v6Fu27lvxv3a@ds129733.mlab.com:29733/tododb';

let dbUrl;
switch(NODE_ENV) {
	case 'development':
		dbUrl = developmentDbUrl;
		break;
	case 'local':
		dbUrl = localDbUrl;
		break;
	case 'test':
		dbUrl = testDbUrl;
		break;
	case 'production':
		dbUrl = productionDbUrl;
		break;
	case 'rpi':
		dbUrl = productionDbUrl;
		break;
	default:
		dbUrl = developmentDbUrl;
}

module.exports = {
  secret: 'R}<Q[x=rZ?58iwHG?JFW6qd<BUht7{sQU5?PMoCrmhHqV!W%Rdaz0|d=W1>.Z<D<',
  database: 'mongodb://appdbuser:v6Fu27lvxv3a@ds129733.mlab.com:29733/tododb'
};


// 'mongodb://appdbuser:v6Fu27lvxv3a@ds129723.mlab.com:29723/todo-api-db'
// 'mongodb://appdbuser:v6Fu27lvxv3a@ds129733.mlab.com:29733/tododb'

